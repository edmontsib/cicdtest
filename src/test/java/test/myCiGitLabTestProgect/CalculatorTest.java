package test.myCiGitLabTestProgect;

import org.junit.Test;

import static org.junit.Assert.*;

public class CalculatorTest {
    //������� ������ ������ �����������
    Calculator calculator = new Calculator();

    //����� �� ������� �������� �����, ������� ����� ����������� ����� sum(int x, int y)
    @Test
    public void sum() {
        int actual = 25;

//����� �� ���������, ��� ��� �������� ����� 10 � 15 ����������� ����� ������ 25
        assertEquals(calculator.sum(10, 15), actual);
    }

    @Test
    public void multiplication() {
        int actual = 30;
//����������, ��� ����������� ����� ��� ������������ ����� 5 � 6 ������ 30
        assertEquals(calculator.multiplication(5, 6), actual);
    }
}